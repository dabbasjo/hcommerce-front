import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { LanguageChangeService } from 'app/services/language-change-service.service';
import { AuthServiceService } from 'app/services/auth-service.service';

@Component({
  moduleId: module.id,
  selector: 'navbar',
  templateUrl: 'navbar.component.html',
  styleUrls: ['navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  @Input() title: string;
  @Input() openedSidebar: boolean = false;
  @Output() sidebarState = new EventEmitter();

  username: string;

  constructor(private languageChangeService : LanguageChangeService, public authService: AuthServiceService) {}

  open(event) {
    let clickedComponent = event.target.closest('.nav-item');
    let items = clickedComponent.parentElement.children;

    for (let i = 0; i < items.length; i++) {
      items[i].classList.remove('opened');
    }
    clickedComponent.classList.add('opened');
  }

  close(event) {
    let clickedComponent = event.target;
    let items = clickedComponent.parentElement.children;

    for (let i = 0; i < items.length; i++) {
      items[i].classList.remove('opened');
    }
  }

  openSidebar() {
    this.openedSidebar = !this.openedSidebar;
    this.sidebarState.emit();
  }

  changeLang() {
    this.languageChangeService.changeAppLang();
  }

  logout(){
    this.authService.logoutUser();
  }

  ngOnInit() {
    this.username = this.authService.getLoggedInUser().firstName;
  }
}