import { Component } from '@angular/core';
import { AuthServiceService } from 'app/services/auth-service.service';

@Component({
  moduleId: module.id,
  selector: 'logo',
  templateUrl: 'logo.component.html',
  styleUrls: ['logo.component.scss']
})
export class LogoComponent {

  imageUrl: string;

  constructor(private authService: AuthServiceService){

    this.imageUrl = this.authService.getLoggedInUser().merchant.imgUrl;

    console.log("this.imageUrl: ",this.imageUrl);

  }

}