import { Component, OnInit } from '@angular/core';
import { LanguageChangeService } from './services/language-change-service.service';
import { Router } from '@angular/router';

@Component({
  moduleId: module.id,
  selector: 'app',
  template: `<router-outlet></router-outlet>`
})
export class AppComponent implements OnInit{

  constructor(private router: Router,private languageChangeService : LanguageChangeService) {

  }

  ngOnInit() {
    this.languageChangeService.initAppLang();
  }

}
