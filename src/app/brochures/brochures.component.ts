import { Component, OnInit } from '@angular/core';
import { SharedService } from 'app/layouts/shared-service';
import { HttpService } from 'app/services/http.service';
import { LanguageChangeService } from 'app/services/language-change-service.service';
import { AuthServiceService } from 'app/services/auth-service.service';

@Component({
  selector: 'app-brochures',
  templateUrl: './brochures.component.html',
  styleUrls: ['./brochures.component.scss']
})
export class BrochuresComponent implements OnInit {
  merchants: any  = []
  pageTitle: string = 'Brochures';
  private request_options: any = {
    method: "",
    path: "",
    body: ''
};

  constructor(private _sharedService: SharedService, private httpService: HttpService,public languageChangeService: LanguageChangeService, private authService: AuthServiceService) {

    this._sharedService.emitChange(this.pageTitle);

   }
  async ngOnInit() {
    this.request_options.path = "brochure/"+this.authService.getLoggedInUser().merchant.id;
    this.request_options.method = "GET";
    let response = await this.httpService.http_request(this.request_options);
    if (response.status == 200) {
        this.merchants = response.body;
        console.log("hhhhhhhhhhhhhhhhhhhh",this.merchants);
    }

  }
  

}
