import { Component, OnInit } from '@angular/core';
import { SharedService } from 'app/layouts/shared-service';
import { HttpService } from 'app/services/http.service';
import { LanguageChangeService } from 'app/services/language-change-service.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-brochure-details',
  templateUrl: './brochure-details.component.html',
  styleUrls: ['./brochure-details.component.scss']
})
export class BrochureDetailsComponent implements OnInit {
  pageTitle: string = 'Brochure Details';

  brochureId: any;

  brochure: any;

  private request_options: any = {
    method: "",
    path: "",
    body: ''
  };

  staticData = [];

  numberOfViews: number;

  names =  ["Abdallah", "Jehad", "Naser", "Omar", "Wessam", "Bashar", "khalid"];

  ages = ["22", "29", "32", "54", "33", "42", "23"];

  citis : any = [];


  constructor(private _sharedService: SharedService, private httpService: HttpService, public languageChangeService: LanguageChangeService, private route: ActivatedRoute) {

    this._sharedService.emitChange(this.pageTitle);
  }

  async ngOnInit() {
    this.brochureId = this.route.snapshot.params['id'];
    console.log('this.brochureId', this.brochureId);
    this.request_options.path = "brochure/find/" + this.brochureId;
    this.request_options.method = "GET";
    let response = await this.httpService.http_request(this.request_options);
    if (response.status == 200) {
      this.brochure = response.body;
    }

    await this.loadCity(2);

    for (var i = 0; i < this.brochure.views; i++) {
      let view = {
        name: this.names[Math.floor(Math.random() * this.names.length)],
        age: this.ages[Math.floor(Math.random() * this.ages.length)],
        gender: "Male",
        city: (this.languageChangeService.direction_layout === 'right') ? (this.citis[Math.floor(Math.random() * this.citis.length)].nameAR) : (this.citis[Math.floor(Math.random() * this.citis.length)].nameEN)
      }
      console.log('view', view);
      this.staticData.push(view);
    }

    console.log('this.staticData', this.staticData);
  }

  async loadCity(areaId) {
    this.request_options.path = "system/city/" + areaId;
    this.request_options.method = "GET";
    let response = await this.httpService.http_request(this.request_options);
    if (response.status == 200) {
      this.citis = response.body;
    }
  }

}
