import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { environment } from '../../environments/environment';
import { CookieService } from 'ngx-cookie-service';
import { AuthServiceService } from './auth-service.service';
import { NgProgress } from 'ngx-progressbar';

@Injectable()
export class HttpService {
  private apiUrl = environment.apiUrl;

  private headers: Headers = new Headers();
  private options: RequestOptions = new RequestOptions();
  private path;
  private prefix = environment.prefix;

  constructor(private cookieService: CookieService, private http: Http, private authService: AuthServiceService, private ngProgress: NgProgress) {
  }

  setHeader(key: string, value: string) {
    this.headers.set(key,value);
  }

  removeHeader(key: string) {
    this.headers.delete(key);
  }

  async http_request(request_options: any): Promise<any> {
    this.ngProgress.start();
    this.options.headers = this.headers;
    this.options.method = request_options.method;
    if (request_options.body) {
      this.options.body = request_options.body;
    }
    if (request_options.prefix) {
      this.prefix = request_options.prefix;
    }
    this.path = request_options.path;
    let http_response = await this.http.request(this.apiUrl + this.prefix + this.path, this.options)
      .toPromise()
      .then(response => response.json())
      .catch((err: any) => {
        let response: any;
        switch (err.status) {
          case 401:
            this.authService.logoutUser();
            response = err.json();
            break;
          case 500:
            response = { status: err.status, message_key: err.statusText }
            break;
          default:
            response = err;
            break;
        }

        //this.prefix = "spac/";
        return response;
      });
      this.ngProgress.done();
    return http_response;
  }

  async getJSON(name: string): Promise<any> {
    let http_response = await this.http.get("assets/data/" + name).toPromise().then(response => response);
    return http_response.json();
  }
}
