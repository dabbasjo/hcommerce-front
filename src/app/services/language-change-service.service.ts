import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { CookieService } from 'ngx-cookie-service';
import { Observable } from 'rxjs';

@Injectable()
export class LanguageChangeService {

    public direction_layout : string = 'right';
    public direction : string = 'rtl';

    constructor(public translate: TranslateService,
         private cookieService: CookieService) {
    }

    initAppLang(): void{
        let lang = (this.cookieService.check('lng')) ? this.cookieService.get('lng') : 'ar';
        this.doChanges(lang);
    }

    changeAppLang(): void{
        let lang = (this.cookieService.get('lng') == 'en') ? 'ar' : 'en';
        this.doChanges(lang);
    }

    doChanges(lang){
        this.setTranslateLang(lang);
        this.setLangCookie(lang);
        this.setAppDirections();
    }

    setTranslateLang(lang: string): void {
        this.translate.use(lang);
    }

    setLangCookie(lang: string): void {
        this.cookieService.set( 'lng', lang );
    }

    setAppDirections(): void {
        this.direction = (this.cookieService.get('lng') == "en") ? "ltr" : "rtl";
        this.direction_layout = (this.cookieService.get('lng') == "ar") ? "right" : "left";
    }

     async getTranslate(key: string): Promise<any> {
       return await this.translate.get(key).toPromise().then(response => response);
     }
}
