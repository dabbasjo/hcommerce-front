import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';


@Injectable()
export class AuthServiceService {

  constructor(private cookieService: CookieService, private router: Router) { }

  private redirectUrl: string = '';
  private loginUrl: string = '/extra-layout/sign-in';
  private isloggedIn: boolean = false;
  private loggedInUser: any;
  private loggedInUserMenue: any;


  isUserLoggedIn(): boolean {
    return this.isloggedIn;
  }

  getRedirectUrl(): string {
    return this.redirectUrl;
  }

  setRedirectUrl(url: string): void {
    this.redirectUrl = url;
  }

  setIsUserLoggedIn(isloggedIn: boolean): void {
    this.isloggedIn = isloggedIn;
  }

  setLoggedInUser(user: any): void {
    this.loggedInUser = user;
  }

  getLoginUrl(): string {
    return this.loginUrl;
  }

  setLoginUrl(url: string): void {
    this.loginUrl = url;
  }

  getLoggedInUser(): any {
    return this.loggedInUser;
  }

  setLoggedInUserMenue(menue: any): void {
    this.loggedInUserMenue = menue;
  }

  getLoggedInUserMenue(): any {
    return this.loggedInUserMenue;
  }

  logoutUser(): void {
    this.isloggedIn = false;
    this.cookieService.deleteAll();
    this.router.navigate([this.getLoginUrl()]);
  }

}
