import { Component, OnInit, Input } from '@angular/core';
import { SharedService } from '../shared-service';
import { LanguageChangeService } from 'app/services/language-change-service.service';

@Component({
  moduleId: module.id,
  selector: 'default-layout',
  templateUrl: 'default.component.html',
  styleUrls: ['default.component.scss'],
  providers: [ SharedService ]
})
export class DefaultLayoutComponent implements OnInit {
  pageTitle: any;
  @Input() openedSidebar: boolean;

  constructor(private _sharedService: SharedService, public languageChangeService : LanguageChangeService) {
    this.openedSidebar = false;

    _sharedService.changeEmitted$.subscribe(
      title => {
        this.pageTitle = title;
        this.openedSidebar = false;
      }
    );
  }

  ngOnInit() { }

  getEnClasses() {
    return {
      'open-sidebar': this.openedSidebar,
      'ltr':true
    };
  }

  getArClasses() {
    return {
      'open-sidebar': this.openedSidebar,
      'rtl': true
    };
  }

  sidebarState() {
    this.openedSidebar = !this.openedSidebar;
  }
}