import { Component } from '@angular/core';
import { LanguageChangeService } from 'app/services/language-change-service.service';

@Component({
  moduleId: module.id,
  selector: 'extra-layout',
  templateUrl: 'extra.component.html',
  styleUrls: ['extra.component.scss']
})
export class ExtraLayoutComponent {
  rtl: boolean = false;

  constructor(public languageChangeService: LanguageChangeService) { }
}