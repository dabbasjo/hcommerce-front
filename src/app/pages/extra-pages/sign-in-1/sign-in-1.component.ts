import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';
import { HttpService } from 'app/services/http.service';
import { AuthServiceService } from 'app/services/auth-service.service';
import { LanguageChangeService } from 'app/services/language-change-service.service';


@Component({
  selector: 'page-sign-in-1',
  templateUrl: './sign-in-1.component.html',
  styleUrls: ['./sign-in-1.component.scss']
})
export class PageSignIn1Component implements OnInit {

  loginForm: FormGroup;

  users: any = [];

  private request_options: any = {
    method: "POST",
    path: "",
    body: ''
  };

  invalidLogin : boolean = false;
  invalidLoginTitle: string;
  invalidLoginBody: string;

  constructor(private router: Router, private cookieService: CookieService, private httpService: HttpService, private authService: AuthServiceService, private languageChangeService: LanguageChangeService) { }

  async ngOnInit() {

    this.loginForm = new FormGroup({
      
      merchantId: new FormControl('', [
        Validators.required
      ]),

      username: new FormControl('', [
        Validators.required
      ]),
      password: new FormControl('', [
        Validators.required,

      ]),
    });

    this.request_options.path = 'merchant/all';

    this.request_options.method = 'Get';

    let response = await this.httpService.http_request(this.request_options);

    if (response.status == 200) {

      this.users = response.body;
      
    }




  }



  async onSubmit() {

    console.log("this.form: ", this.loginForm.value);




    if (this.loginForm.valid) {

      this.request_options.path = 'merchant/login';

      this.request_options.method = 'POST';

      this.request_options.body = this.loginForm.value;




      let response = await this.httpService.http_request(this.request_options);
      if (response.status == 200) {
        this.cookieService.set("token", JSON.stringify(response.body));
        this.authService.setLoggedInUser(response.body);
        this.invalidLogin = false;
        this.router.navigate(['default-layout/brochure-profile']);
      }else{
        this.invalidLogin = true;
        this.invalidLoginTitle = "Error";
        this.invalidLoginBody = "Invalid_Login"
      }


    }

  }

  changeLang() {
    this.languageChangeService.changeAppLang();
  }
}
