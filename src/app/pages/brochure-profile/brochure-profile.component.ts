import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormControl, FormGroup, Validators, ValidatorFn, AbstractControl } from '@angular/forms';

import { map, filter, catchError, mergeMap } from 'rxjs/operators';

import { SharedService } from '../../layouts/shared-service';
import { LanguageChangeService } from 'app/services/language-change-service.service';
import { HttpService } from 'app/services/http.service';
import { AuthServiceService } from 'app/services/auth-service.service';
import { MatDialog, MatDialogRef, MatDialogConfig } from '@angular/material';
@Component({
  selector: 'app-brochure-profile-elements',
  templateUrl: './brochure-profile.component.html',
  styleUrls: ['./brochure-profile.component.scss']
})
export class BrochureProfileComponent implements OnInit {
  pageTitle: string = 'Create_Brochure';
  stateCtrl: FormControl;
  filteredStates: any;

  dialogRef: MatDialogRef<DialogBrochureComponent>;
  selectedOption: string;

  brochureForm: FormGroup;

  brochureItemList: any = [];
  counter: number = 1;

  @ViewChild('fileInput') fileInput: ElementRef;

  private request_options: any = {
    method: "POST",
    path: "",
    body: ''
  };

  imageLoaded: boolean = false;
  url: any;

  states = [
    'Alabama',
    'Alaska',
    'Arizona',
    'Arkansas',
    'California',
    'Colorado',
    'Connecticut',
    'Delaware',
    'Florida',
    'Georgia',
    'Hawaii',
    'Idaho',
    'Illinois',
    'Indiana',
    'Iowa',
    'Kansas',
    'Kentucky',
    'Louisiana',
    'Maine',
    'Maryland',
    'Massachusetts',
    'Michigan',
    'Minnesota',
    'Mississippi',
    'Missouri',
    'Montana',
    'Nebraska',
    'Nevada',
    'New Hampshire',
    'New Jersey',
    'New Mexico',
    'New York',
    'North Carolina',
    'North Dakota',
    'Ohio',
    'Oklahoma',
    'Oregon',
    'Pennsylvania',
    'Rhode Island',
    'South Carolina',
    'South Dakota',
    'Tennessee',
    'Texas',
    'Utah',
    'Vermont',
    'Virginia',
    'Washington',
    'West Virginia',
    'Wisconsin',
    'Wyoming',
  ];

  favoriteSeason: string = 'Winter';

  seasons: string[] = [
    'Winter',
    'Spring',
    'Summer',
    'Autumn'
  ];

  color: string;

  availableColors = [
    { name: 'Default', color: '' },
    { name: 'Primary', color: 'primary' },
    { name: 'Accent', color: 'accent' },
    { name: 'Warning', color: 'warn' }
  ];

  constructor(private _sharedService: SharedService, private languageChangeService: LanguageChangeService, private httpService: HttpService, private authService: AuthServiceService, public dialog: MatDialog) {
    this.loadForm();
    this.stateCtrl = new FormControl();
    this.filteredStates = this.stateCtrl.valueChanges.pipe(
      map(name => this.filterStates(name))
    );

    this._sharedService.emitChange(this.pageTitle);
  }

  filterStates(val: string) {
    return val ? this.states.filter((s) => new RegExp(val, 'gi').test(s)) : this.states;
  }

  async ngOnInit() {
    this.url = 'assets/img/default.png';
  }

  onFileBrochureChange(event) {
    let reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.brochureForm.get('coverPhoto').setValue(reader.result)
        this.imageLoaded = true;
        this.url = reader.result;
      };
    }
  }

  openDialog(result, message) {
    let dialogRef = this.dialog.open(DialogBrochureComponent);
    dialogRef.componentInstance.result = result;
    dialogRef.componentInstance.mesasge = message;
    dialogRef.afterClosed().subscribe(result => {
    });
  }

  onFileItemChange(event, currentItem) {
    let reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {
        let itemIndex = this.brochureItemList.findIndex(item => item.counter === currentItem.counter);
        this.brochureItemList[itemIndex].imageURL = reader.result;
      };
    }
  }

  clearBrochureFile() {
    this.fileInput.nativeElement.value = '';
    this.imageLoaded = false;
    this.url = 'assets/img/default.png';
  }

  loadForm() {
    this.brochureForm = new FormGroup({
      nameAr: new FormControl('', [Validators.required]),
      nameEn: new FormControl('', [Validators.required]),
      expirationDate: new FormControl('', [Validators.required]),
      startDate: new FormControl('', [Validators.required]),
      descriptionAr: new FormControl('', [Validators.required]),
      descriptionEn: new FormControl('', [Validators.required]),
      coverPhoto: new FormControl('')
    });
  }

  async onSubmit() {
    this.markFormGroupTouched();
    this.brochureForm.value.brochureItemList = this.brochureItemList;
    this.brochureForm.value.merchantUserId = this.authService.getLoggedInUser().id;
    console.log(this.brochureForm.value);
    if (this.brochureForm.valid) {
      this.request_options.path = 'brochure/save';

      this.request_options.method = 'POST';

      this.request_options.body = this.brochureForm.value;

      let response = await this.httpService.http_request(this.request_options);
      console.log(response)
      if (response.status == 200) {
        this.brochureItemList = [];
        this.url = 'assets/img/default.png';
        this.brochureForm.reset();
        this.openDialog('Success', 'Operation_Done_Successfully');
      } else {
        this.openDialog('Error', response.json().msgAR);
      }
    }
  }

  private markFormGroupTouched() {
    (<any>Object).values(this.brochureForm.controls).forEach(control => {
      control.markAsTouched();
    });
  }

  addItem() {

    let counter = this.counter++;

    let item = {
      counter: counter,
      title: "Item",
      imageURL: "assets/img/default.png"
    }

    this.brochureItemList.push(item);
    this.brochureForm.addControl('itemNameAr' + item.counter, new FormControl('', [Validators.required]));
    this.brochureForm.addControl('itemNameEn' + item.counter, new FormControl('', [Validators.required]));
    this.brochureForm.addControl('itemOldPrice' + item.counter, new FormControl('', [Validators.required]));
    this.brochureForm.addControl('itemNewPrice' + item.counter, new FormControl('', [Validators.required, this.lessThan('itemOldPrice' + item.counter)]));
    this.brochureForm.addControl('itemDescAr' + item.counter, new FormControl('', [Validators.required]));
    this.brochureForm.addControl('itemDescEn' + item.counter, new FormControl('', [Validators.required]));
    this.brochureForm.addControl('itemColorAr' + item.counter, new FormControl('', [Validators.required]));
    this.brochureForm.addControl('itemColorEn' + item.counter, new FormControl('', [Validators.required]));
    this.brochureForm.addControl('itemSizeAr' + item.counter, new FormControl('', [Validators.required]));
    this.brochureForm.addControl('itemSizeEn' + item.counter, new FormControl('', [Validators.required]));
  }

  setItem(currentItem) {
    let itemIndex = this.brochureItemList.findIndex(item => item.counter === currentItem.counter);
    this.brochureItemList[itemIndex].itemNameAr = this.brochureForm.get('itemNameAr' + currentItem.counter).value;
    this.brochureItemList[itemIndex].itemNameEn = this.brochureForm.get('itemNameEn' + currentItem.counter).value;
    this.brochureItemList[itemIndex].oldPrice = this.brochureForm.get('itemOldPrice' + currentItem.counter).value;
    this.brochureItemList[itemIndex].newPrice = this.brochureForm.get('itemNewPrice' + currentItem.counter).value;
    this.brochureItemList[itemIndex].descriptionAr = this.brochureForm.get('itemDescAr' + currentItem.counter).value;
    this.brochureItemList[itemIndex].descriptionEn = this.brochureForm.get('itemDescEn' + currentItem.counter).value;
    this.brochureItemList[itemIndex].colorAr = this.brochureForm.get('itemColorAr' + currentItem.counter).value;
    this.brochureItemList[itemIndex].colorEn = this.brochureForm.get('itemColorEn' + currentItem.counter).value;
    this.brochureItemList[itemIndex].sizeAr = this.brochureForm.get('itemSizeAr' + currentItem.counter).value;
    this.brochureItemList[itemIndex].sizeEn = this.brochureForm.get('itemSizeEn' + currentItem.counter).value;

  }

  clickFileItem(item) {
    document.getElementById(('itemImage') + (item.counter)).click();
  }


  lessThan(field_name): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {

      let input = control.value;

      let isValid = control.root.value[field_name] > input;
      if (!isValid)
        return { 'lessThan': { isValid } }
      else
        return null;
    };
  }
}
@Component({
  selector: 'dialog-brochure-profile',
  templateUrl: 'dialog-brochure-profile.html',
})
export class DialogBrochureComponent {
  jazzMessage = 'Jazzy jazz jazz';
  result: any;
  mesasge: any;
  constructor(public dialogRef: MatDialogRef<DialogBrochureComponent>) { }
}
