import { Component, OnInit } from '@angular/core';
import { SharedService } from 'app/layouts/shared-service';
import { HttpService } from 'app/services/http.service';
import { LanguageChangeService } from 'app/services/language-change-service.service';
import { AuthServiceService } from 'app/services/auth-service.service';
import { Options } from 'ng5-slider';
import * as $ from 'jquery';

@Component({
  selector: 'app-brochures-notification',
  templateUrl: './brochures-notification.component.html',
  styleUrls: ['./brochures-notification.component.scss']
})
export class BrochuresNotificationsComponent implements OnInit {
  merchants: any = [];
  areas: any = [];
  cities: any = [];
  districts: any = [];
  nationalities: any = [];

  selectedBrochure: any;

  minValue: number = 20;
  maxValue: number = 80;
  options: Options = {
    floor: 18,
    ceil: 100,
    step: 1
  };

  pageTitle: string = 'Push_Notification';
  private request_options: any = {
    method: "",
    path: "",
    body: ''
  };

  constructor(private _sharedService: SharedService, private httpService: HttpService, public languageChangeService: LanguageChangeService, private authService: AuthServiceService) {

    this._sharedService.emitChange(this.pageTitle);

  }
  async ngOnInit() {
    this.request_options.path = "brochure/" + this.authService.getLoggedInUser().merchant.id;
    this.request_options.method = "GET";
    let response = await this.httpService.http_request(this.request_options);
    if (response.status == 200) {
      this.merchants = response.body;
    }

    await this.loadArea();

    await this.loadNationalities();
  }

  async loadNationalities() {
    this.nationalities = await this.httpService.getJSON('CountryCodes.json');
  }

  async loadArea() {
    this.request_options.path = "system/area";
    this.request_options.method = "GET";
    let response = await this.httpService.http_request(this.request_options);
    if (response.status == 200) {
      this.areas = response.body;
    }
  }


  async loadCity(areaId) {
    $("#selectCity").val("");
    $("#selectDistrict").val("");
    this.request_options.path = "system/city/" + areaId;
    this.request_options.method = "GET";
    let response = await this.httpService.http_request(this.request_options);
    if (response.status == 200) {
      this.cities = response.body;
    }
  }

  async loadDistrict(districtId) {
    $("#selectDistrict").val("");
    this.request_options.path = "system/district/" + districtId;
    this.request_options.method = "GET";
    let response = await this.httpService.http_request(this.request_options);
    if (response.status == 200) {
      this.districts = response.body;
    }
  }



  async sendNotification() {
    this.request_options.path = "brochure/notify/" + this.selectedBrochure.id;
    this.request_options.method = "GET";
    let response = await this.httpService.http_request(this.request_options);
    if (response.status == 200) {
    }
  }

  openModal(id: string, brochure) {
    // this.modalService.open(id);
    this.selectedBrochure = brochure;
    document.querySelector('#' + id).classList.add('md-show');
  }

  closeModal(id: string) {
    // this.modalService.close(id);
    document.querySelector('#' + id).classList.remove('md-show');
  }

  saveModal() {
    this.sendNotification();
    this.closeModal('custom-modal-1');
  }

  resetModal() {
    $("#selectArea").val("");
    $("#selectCity").val("");
    $("#selectDistrict").val("");
    $("#selectNationality").val("");
  }
}
