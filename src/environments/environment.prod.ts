export const environment = {
  production: false,
  apiUrl: 'http://hypercommercespringboot.us-east-2.elasticbeanstalk.com',
  prefix: '/',
  defaultLatitude: 24.67380739,
  defaultLongitude: 46.72992257
};
